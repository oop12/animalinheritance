/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author user
 */
public class Duck extends Animal {

    public Duck(String name, String color) {
        super(name, color, 2);
        System.out.println("Duck created");
    }

    public void fly() {
        System.out.println("Duck: " + name + " can fly!!!");
    }
    public void walk() {
        System.out.println("Duck: " + name + " walk with " + numberOfLegs + " legs.");
    }
     public void speak() {
        System.out.println("Duck: " + name + " speak -> Quack Quack!!!");
    }

}
