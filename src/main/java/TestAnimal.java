/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author user
 */
public class TestAnimal {

    public static void main(String[] args) {
        Animal animal = new Animal("Ani", "Unkown", 0);
        animal.speak();
        animal.walk();
        line();

        Dog dang = new Dog("Dang", "Black&White");
        dang.speak();
        dang.walk();
        line();

        Dog to = new Dog("To", "Brown");
        to.speak();
        to.walk();
        line();

        Dog bat = new Dog("Bat", "White");
        bat.speak();
        bat.walk();
        line();

        Dog mome = new Dog("Mome", "White");
        mome.speak();
        mome.walk();
        line();

        Cat zero = new Cat("Zero", "Orange");
        zero.speak();
        zero.walk();
        line();

        Duck som = new Duck("Som", "Yellow");
        som.speak();
        som.walk();
        som.fly();
        line();

        Duck gabgab = new Duck("GabGab", "Yellow");
        gabgab.speak();
        gabgab.walk();
        gabgab.fly();
        line();
        
        
        System.out.println("+--------Polymorphism--------+");
        Animal[] animals = {dang,to,bat,mome,som,gabgab,zero};
        for(int i=0; i<animals.length; i++){
            animals[i].speak();
            animals[i].walk();
            if(animals[i] instanceof Duck){
                Duck duck = (Duck)animals[i];
                duck.fly();
            }
            line();
        }
        
    }

    private static void line() {
        System.out.println("-------------------");
    }
}
